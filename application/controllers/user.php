<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //checks if session exists. if not, redirects to login page
        if (!$this->session->userdata('id')) {
            redirect('login');
        }
        //loads the necessary library and model
        $this->load->library('form_validation');
        $this->load->model('user_model');
    }

    public function index() {
        //gets the user id
        $user_id = $this->session->userdata('id');
        //lods the get_user function inside the user_model with the given parameter
        $user = $this->user_model->get_user($user_id);
        //gets the user data from the model
        $data['id'] = $user->id;
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        //loads the user information on the page
        $this->load->view('user', $data);
    }
    
    public function send_message() {
        //validates the info given in the input of the form
        $this->form_validation->set_rules('message', 'Message', 'required|trim');
        //if the info is valid
        if ($this->form_validation->run()) {
            //stores the data in the data array
            $data = array(
                'user_id' => $this->session->userdata('id'),
                'message' => $this->input->post('message'),
                'date_added' => date('Y-m-d H:i:s')
            );
            //loads the save_message data inside the user_model
            $id = $this->user_model->save_message($data);
            //if the id exist in the dv
            if ($id > 0) {
                //stores the message in a variable
                $validate = "Thank you for your message!";
                //shows it in the page
                $this->session->set_flashdata('message', $validate);
                redirect('user/message');
            }
        } else {
            //loads the message function
            $this->message();
        }
    }
        
    public function delete_message() {
        //gets the id of the message
        $message_id = $this->uri->segment('3');
        //loads the delete_message function inside the user_model
        $delete = $this->user_model->delete_message($message_id);
        //redirects to the message function inside the user controller
        redirect('user/message');
    }
    
    public function message() {
        //gets the id of the user
        $user_id = $this->session->userdata('id');
        //gets a list of all the messages of a user. loads the get_message function inside the user_model
        $messages_list = $this->user_model->get_messages($user_id);
        //stores the list in an array
        $data = array(
            'messages_list' => $messages_list
        );
        //loads the get_user function inside the user_model
        $user = $this->user_model->get_user($user_id);
        //stores all the data in separate variables
        $data['id'] = $user->id;
        $data['name'] = $user->name;
        $data['email'] = $user->email;
        //sends them to the message view
        $this->load->view('message', $data);
    }

    public function logout() {
        //gets all the sessions that are related to the user and deletes them
        $data = $this->session->all_userdata();
        foreach ($data as $row => $rows_value) {
            $this->session->unset_userdata($row);
        }
        //redirects to login
        redirect('login');
    }

}

?>

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class register extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('id')) {
            redirect('user');
        }

        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model('register_model');
    }
    //loads the register view
    public function index() {
        $this->load->view('register');
    }

    public function validation() {
        //validates all the info from the form inputs
        $this->form_validation->set_rules('user_name', 'Name', 'required|trim');
        $this->form_validation->set_rules('user_email', 'Email address', 'required|trim|valid_email|is_unique[user_register.email]');
        $this->form_validation->set_rules('user_password', 'Password', 'required|trim');
        //if the info is valid
        if ($this->form_validation->run()) {
            //generates random validation key
            $validation_key = md5(rand());
            //encrypts the password given the validation ket
            $encrypted_password = $this->encrypt->encode($this->input->post('user_password'));
            //stores the data from the inputs in the data array
            $data = array(
                'name' => $this->input->post('user_name'),
                'email' => $this->input->post('user_email'),
                'password' => $encrypted_password,
                'validation_key' => $validation_key
            );
            //inserts the data in the db and returns the id
            $id = $this->register_model->insert($data);
            
            if ($id > 0) {
                //returns an url with the validation url that calls the validate_email function within the register controller
                $validate = "This is the validation for Elena's Blog. Please validate your account: <a href='" . base_url() . "register/validate_email/" . $validation_key . "'>link</a>.";
                $this->session->set_flashdata('message', $validate);
                redirect('register');
            }
        } else {
            $this->index();
        }
    }

    public function validate_email() {
        //checks if the validation key exists
        if ($this->uri->segment(3)) {
            //stores the validation key in a variable
            $validation_key = $this->uri->segment(3);
            //loads the validate_email function within the register_model gile
            if ($this->register_model->validate_email($validation_key)) {
                $data['message'] = '<p align="center" class="text-success">Your Account has been successfully validated, now you can login from <a href="' . base_url() . 'login">here</a></p>';
            } else {
                $data['message'] = '<p align="center" class="text-danger">Invalid link or the Account was already validated</p>';
            }
            //loads the email_validation page
            $this->load->view('email_validation', $data);
        }
    }

}

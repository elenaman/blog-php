<?php
//makes sure all codeigniter baseclasses are beign loaded and making sure certain vars have been set
defined('BASEPATH') OR exit('No direct script access allowed');

class posts_list extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //incarca librariile pt validarea formularelor si modelului 
        $this->load->library('form_validation');
        $this->load->model('posts_model');
    }

    function index() {
        //loads the get_posts function inside the posts_model
        $posts_list = $this->posts_model->get_posts();
        
        $data = array(
            'posts_list' => $posts_list
        );
        //loads the posts_list page with the data given
        $this->load->view('posts_list', $data);
    }

    function get_post() {
        //gets the id from the URL
        $post_id = $this->uri->segment('3');
        //get the post from the get_post function inside the posts_model and stores it in the post var
        $post = $this->posts_model->get_post($post_id);
        
        $data['post_id'] = $post->post_id;
        $data['headline'] = $post->headline;
        $data['teaser'] = html_entity_decode($post->teaser, ENT_QUOTES, 'UTF-8');
        $data['content'] = html_entity_decode($post->content, ENT_QUOTES, 'UTF-8');
        $data['category'] = $post->category;
        $data['status'] = $post->status;
        $data['date_edited'] = $post->date_edited;
        $data['date_created'] = $post->date_created;
        //loads the post view with the data given from every variable
        $this->load->view('post', $data);
    }
    //loads a page with all the posts
    public function manage_posts() {
        //loads the get_posts function inside the posts_model and stores it into the posts_list variable
        $posts_list = $this->posts_model->get_posts();
        //creates an array of the posts and stores it in the data variable
        $data = array(
            'posts_list' => $posts_list
        );
        //loads the manage_posts page with the data
        $this->load->view('manage_posts', $data);
    }
    
    public function delete_post() {
        //gets the it of the post to be deleted
        $post_id = $this->uri->segment('3');
        //deletes the post with the desegnated id
        $delete = $this->posts_model->delete_post($post_id);

        redirect('posts_list/manage_posts');
    }
 
    public function add_post() {
        //loads the add_post page
        $this->load->view('add_post');
    }

    public function edit_post() {
        //gets the id of the post
        $post_id = $this->uri->segment('3');
        //stores the post from the db in the post variable
        $post = $this->posts_model->get_post($post_id);

        $data['post_id'] = $post->post_id;
        $data['headline'] = $post->headline;
        $data['teaser'] = html_entity_decode($post->teaser, ENT_QUOTES, 'UTF-8');
        $data['content'] = html_entity_decode($post->content, ENT_QUOTES, 'UTF-8');
        $data['source'] = $post->source;
        $data['source_url'] = $post->source_url;
        //loads the page with the given data from above
        $this->load->view('edit_post', $data);
    }

    public function validate_post() {
        //validates that the data added in the fields are valid
        $this->form_validation->set_rules('headline', 'Headline', 'required|trim');
        $this->form_validation->set_rules('teaser', 'Teaser', 'required|trim');
        $this->form_validation->set_rules('content', 'Content', 'required|trim');
        //if the data is valid
        if ($this->form_validation->run()) {
            //stores the data from the form fields in an array within the data variable
            $data = array(
                'headline' => $this->input->post('headline'),
                'teaser' => $this->input->post('teaser'),
                'content' => $this->input->post('content'),
                'source' => $this->input->post('source'),
                'source_url' => $this->input->post('source_url'),
                'date_created' => date('Y-m-d H:i:s')
            );
            //calls the insert_post function with the data parameter within the posts_model and stores it in the id variable
            $id = $this->posts_model->insert_post($data);
            //if the id exists in the db
            if ($id > 0) {
                //loads the manage_post function within posts_list controller
                redirect('posts_list/manage_posts');
            }
        } else {
            //loads the manage_posts function
            $this->manage_posts();
        }
    }
    
    public function validate_edit() {
        //gets the id of the post
        $post_id = $this->uri->segment('3');
        //checks that the information from the inputs are valid
        $this->form_validation->set_rules('headline', 'Headline', 'required|trim');
        $this->form_validation->set_rules('teaser', 'Teaser', 'required|trim');
        $this->form_validation->set_rules('content', 'Content', 'required|trim');
        //if the information is valid
        if ($this->form_validation->run()) {
            //stores the data in an array
            $data = array(
                'headline' => $this->input->post('headline'),
                'teaser' => $this->input->post('teaser'),
                'content' => $this->input->post('content'),
                'source' => $this->input->post('source'),
                'source_url' => $this->input->post('source_url'),
                'date_created' => date('Y-m-d H:i:s')
            );
            //calls the upgrade_post function and updates the db
            $this->posts_model->update_post($data, $post_id);
            //loads the manage_post function within posts_list controller
            redirect('posts_list/manage_posts');
        } else {
            //loads the mange_post function
            $this->manage_posts();
        }
    }

}

?>
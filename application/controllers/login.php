<?php
//makes sure all codeigniter baseclasses are beign loaded and making sure certain vars have been set
defined('BASEPATH') OR exit('No direct script access allowed');
//defined login class
class login extends CI_Controller {
    //construct functions loads just once 
    public function __construct() {
        parent::__construct();
        //if session exists
        if ($this->session->userdata('id')) {
            //redirect to the user page (login is not necessary since person is already logged in)
            redirect('user');
        }
        //if session does not exist, loads the libraries and models necessary for the login
        $this->load->library('form_validation');
        $this->load->library('encrypt');
        $this->load->model('login_model');
    }
    //loads the login page
    function index() {
        $this->load->view('login');
    }
    //function that validates the email
    function validation() {
        //verifies the email and password are valid
        $this->form_validation->set_rules('user_email', 'Email Address', 'required|trim|valid_email');
        $this->form_validation->set_rules('user_password', 'Password', 'required');
        //if email and pass is valid (run() verifica daca regulile de mai sus sunt indeplinite).
        //ruleaza clasa form validation
        if ($this->form_validation->run()) {
            //loads the login model with the can_login function within the model
            $result = $this->login_model->can_login($this->input->post('user_email'), $this->input->post('user_password'));
            //if there is no result, the function is successful and redirects to user view
            if ($result == '') {
                redirect('user');
            //throws and error message and redirects to login
            } else {
                $this->session->set_flashdata('message', $result);
                redirect('login');
            }
        } else {
            //returns the index function without redirecting
            $this->index();
        }
    }

}

?>
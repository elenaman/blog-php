
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta name="theme-color" content="#ce5169">
        <!--<meta name="viewport" content="height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />-->
        <!--<meta name="viewport" content="initial-scale=1">-->
        <title>Elena's Blog</title>

        <!-- stylesheet -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=devanagari,latin-ext" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/jquery-ui-1.12.1.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">

        <!--[if lt IE 9]>
          <script src="https:/oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https:/oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-inline">
                            <li><a href="<?php echo base_url(); ?>login">Login</a><li>
                            <li><a href="<?php echo base_url(); ?>posts_list">Posts list</a><li>
                            <li><a href="<?php echo base_url(); ?>about">About us</a><li>
                            <li>|</li>
                            <li><a href="<?php echo base_url(); ?>posts_list/manage_posts"><strong>Manage posts</strong></a><li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">

                                <h3 class="card-title">Add post</h3>

                                <form method="post" action="<?php echo base_url() ?>posts_list/validate_post">

                                    <div class="form-group">
                                        <label>Headline</label>
                                        <input type="text" name="headline" class="form-control" value="<?php echo set_value('headline'); ?>">
                                        <span class="text-danger"><?php echo form_error('headline'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Teaser</label>
                                        <textarea name="teaser" class="form-control" rows="5" id="teaser"><?php echo set_value('teaser'); ?></textarea>
                                        <span class="text-danger"><?php echo form_error('teaser'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Content</label>
                                        <textarea name="content" class="form-control" rows="5" id="content"><?php echo set_value('content'); ?></textarea>
                                        <span class="text-danger"><?php echo form_error('content'); ?></span>
                                    </div>

                                    <div class="form-group">
                                        <label>Source</label>
                                        <input type="text" name="source" class="form-control" value="<?php echo set_value('source'); ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Source URL</label>
                                        <input type="text" name="source_url" class="form-control" value="<?php echo set_value('source_url'); ?>">
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="register" class="btn btn-info" value="Save">
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!-- #page-wrapper -->

            <!-- jquery -->
            <script src="<?php echo base_url(); ?>public/js/jquery-3.2.1.min.js"></script> 
            <script src="<?php echo base_url(); ?>public/js/jquery-ui-1.12.1.min.js"></script>
            <script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script> 

            <script src="<?php echo base_url(); ?>public/js/ckeditor/ckeditor.js"></script>

            <script type="text/javascript">
                CKEDITOR.replace('teaser');
                CKEDITOR.replace('content');
            </script>

    </body>
</html>
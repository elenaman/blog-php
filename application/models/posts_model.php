<?php

class posts_model extends CI_Model {
    //gets all posts from the db
    function get_posts() {
        
        $query = $this->db->get('blog_posts');

        return $query->result();
        
    }
    //gets the desired post from the db where it has the given id
    function get_post($post_id) {
  
        $this->db->where('post_id', $post_id);
        $query = $this->db->get('blog_posts');

        return $query->row();
        
    }
    //deletes the post from db where it has the desired id
    function delete_post($post_id) {
        
        $this->db->where('post_id', $post_id);
        $this->db->delete('blog_posts');
        
    }
    //inserts new post in the db given the necessary data
    function insert_post($data) {
        
        $this->db->insert('blog_posts', $data);
        return $this->db->insert_id();
        
    }
    //updates the post after modifications
    function update_post($data, $post_id) {
        
        $this->db->where('post_id', $post_id);
        $this->db->update('blog_posts', $data);
        
    }

}

?>
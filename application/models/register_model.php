<?php

class register_model extends CI_Model {
    //inserts the data into the user_register table
    public function insert($data) {
        
        $this->db->insert('user_register', $data);
        return $this->db->insert_id();
        
    }
    //validates the email
    public function validate_email($key) {
        //selects from the rows in the db where the validation key exists and the account is not validated yet
        $this->db->where('validation_key', $key);
        $this->db->where('is_validated', 0);
        //gets the row from the db where the above conditions are met
        $query = $this->db->get('user_register');
        
        if ($query->num_rows() > 0) {
           
            $data = array(
                'is_validated' => 1
            );
            //updates the user_register table with the data given from the data variable
            $this->db->where('validation_key', $key);
            $this->db->update('user_register', $data);
            return true;
        } else {
            return false;
        }
    }

}

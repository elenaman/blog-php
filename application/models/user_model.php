<?php

class user_model extends CI_Model {
    //gets the user data from the db
    public function get_user($user_id) {
        //looks for an entry which has the given id
        $this->db->where('id', $user_id);
        //selects it from the database
        $query = $this->db->get('user_register');
        //and returns it
        return $query->row();
    }
    //inserts messages in the db
    public function save_message($data) {
        //inserts the message in the db given the data in the user controller
        $this->db->insert('our_messages', $data);
        return $this->db->insert_id();
    }
    //retrieves the messages of a certain user given the user id
    function get_messages($user_id) {
        //looks for an entry which has a given id
        $this->db->where('user_id', $user_id);
        //selects it from the db
        $query = $this->db->get('our_messages');
        //and returns it
        return $query->result();
    }
    //deletes a message from the db given the message id
    function delete_message($message_id) {
        //lookds for an entry with the given id
        $this->db->where('message_id', $message_id);
        //deletes it from the db
        $this->db->delete('our_messages');
        

    }

}

?>
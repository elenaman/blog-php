<?php

class login_model extends CI_Model {

    function can_login($email, $password) {
        //verifica daca emailul introdus exista in db
        $this->db->where('email', $email);
        $query = $this->db->get('user_register');
        //daca emailul exista
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                //daca email-ul e validat
                if ($row->is_validated == 1) {
                    //extrage parola din db
                    $store_password = $this->encrypt->decode($row->password);
                    //compara parola din db cu cea din formular
                    if ($password == $store_password) {
                        //creeaza o sesiune noua
                        $this->session->set_userdata('id', $row->id);
                    } else {
                        return 'Wrong Password';
                    }
                } else {
                    return 'First verify your email address';
                }
            }
        } else {
            return 'Wrong Email Address';
        }
        
    }

}

?>